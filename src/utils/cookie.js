import Cookies from 'js-cookie';

const cookies = Cookies.withConverter({
  read (value, name) {
    if (name === 'UserToken') {
      return decodeURI(value)
    }
    return Cookies.converter.read(value, name)
  }
});

export default cookies;
