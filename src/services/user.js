import request from '@/utils/request';

export async function query(params) {
  return request('/api/user/list', {
    method: 'POST',
    data: params
  })
}
export async function queryCurrent() {
  return request('/api/user/current');
}
export async function queryNotices() {
  return request('/api/notices');
}

export async function addPoints(params) {
  return request('/api/user/addpoints', {
    method: 'POST',
    data: params
  })
}
