import request from "@/utils/request";


export async function addRbZanTask (params) {
  return request('/api/redBookZan/add', {
    method: 'POST',
    data: params
  })
}

export async function getZanList(params) {
  return request('/api/redBookZan/list', {
    method: 'POST',
    data: params
  })
}

export async function startZanTask(id) {
  return request(`/api/redBookZan/start?id=${id}`, {
    method: 'POST'
  })
}

export async function pauseZanTask(id) {
  return request(`/api/redBookZan/pause?id=${id}`, {
    method: 'POST'
  })
}

export async function deleteZanTask(id) {
  return request(`/api/redBookZan/delete?id=${id}`, {
    method: 'POST'
  })
}
