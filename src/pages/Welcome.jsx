import React from 'react';
import {  GridContent } from '@ant-design/pro-layout';
import { Card, Col, Row, Tabs  } from 'antd';
const { TabPane } = Tabs;

export default () => (
  <GridContent>
    <Row
      gutter={24}
      style={{
        marginTop: 24,
      }}
    >
      <Col xl={8} lg={24} md={24} sm={24} xs={24}>
        <Card title="剩余金额" bordered={false}>
          10000
        </Card>
      </Col>
      <Col xl={8} lg={24} md={24} sm={24} xs={24}>
        <Card title="剩余任务" bordered={false}>
          10000
        </Card>
      </Col>
      <Col xl={8} lg={24} md={24} sm={24} xs={24}>
        <Card title="累计消费" bordered={false}>
          10000
        </Card>
      </Col>

      <Col xl={24} lg={24} md={24} sm={24} xs={24} style={{marginTop: 24}}>
        <Card>
          <Tabs defaultActiveKey="1">
            <TabPane tab="消费记录" key="1">
              消费记录
            </TabPane>
            <TabPane tab="充值记录" key="2">
              充值记录
            </TabPane>
          </Tabs>
        </Card>
      </Col>
    </Row>

  </GridContent>
);
