import React, {useEffect} from "react";
import {Modal, Form, Input, InputNumber } from 'antd';

const FormItem = Form.Item;
const formLayout = {
  labelCol: {
    span: 7,
  },
  wrapperCol: {
    span: 13,
  },
};

const AddPoints = props => {
  const [form] = Form.useForm();
  const {
    modalVisible,
    onCancel: handleCancel,
    onOk: handleOk,
    Id,
    Name
  } = props;

  const handleModalOk = async () => {
    let fieldsValue = await form.validateFields();
    fieldsValue = {...fieldsValue, ...{Id}};
    handleOk(fieldsValue);
  };

  useEffect(() => {
    if (modalVisible) {
      form.resetFields();
    }
  }, [modalVisible]);

  return (
    <Modal
      destroyOnClose
      title="充值积分"
      visible={modalVisible}
      onCancel={() => {
        form.resetFields();
        handleCancel();
      }}
      onOk={() => handleModalOk()}
    >
      <Form
        {...formLayout}
        form={form}
        initialValues={{
          Name
        }}
      >
        <FormItem
          name="Name"
          label="用户"
        >
          <Input placeholder="用户" disabled/>
        </FormItem>
        <FormItem
          name="Points"
          label="充值金额"
          rules={[
            {
              required: true,
              message: '请输入金额',
            },
          ]}
        >

          <InputNumber placeholder="金额" />
        </FormItem>

      </Form>
    </Modal>
  );
};
export default AddPoints;
