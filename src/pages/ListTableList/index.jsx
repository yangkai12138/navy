import {PlusOutlined} from '@ant-design/icons';
import {Button, message} from 'antd';
import React, {useState, useRef, useMemo} from 'react';
import {PageHeaderWrapper} from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import {query, addPoints} from '@/services/user';
import AddPoints from "@/pages/ListTableList/components/AddPoints";

const TableList = () => {
  const actionRef = useRef();

  const [addPointsVisible, setAddPointsVisible] = useState(false);
  const [id, setId] = useState('');
  const [name, setName] = useState('');

  const columns = [
    {
      title: '用户名',
      dataIndex: 'Name',
      width: '40%'
    },
    {
      title: '积分',
      dataIndex: 'Points',
      hideInSearch: true,
      width: '40%'
    },
    {
      title: '操作',
      dataIndex: 'option',
      valueType: 'option',
      width: '20%',
      render: (_, record) => {
        const {Name, Id} = record;
        return (
          <>
            <Button
              type="primary"
              size="small"
              onClick={() => {
                setId(Id);
                setName(Name);
                setAddPointsVisible(true)
              }}
            >
              充值积分
            </Button>
          </>
        )
      }
    },
  ];


  const memoAddPoints = useMemo(() => {
    return (
      <AddPoints
        Id={id}
        Name={name}
        modalVisible={addPointsVisible}
        onCancel={() => setAddPointsVisible(false)}
        onOk={async values => {
          const res = await addPoints(values);
          const {status, msg} = res;
          if (status === 1) {
            message.success(msg);
            setAddPointsVisible(false);
            actionRef.current.reload();
          } else {
            message.error(msg);
          }
        }}
      />
    )
  }, [addPointsVisible]);

  return (
    <PageHeaderWrapper>
      <ProTable
        headerTitle=""
        actionRef={actionRef}
        options={[]}
        pagination={{
          pageSize: 20,
          showSizeChanger: false
        }}
        tableAlertRender={false}
        rowKey={record => record.Id}
        toolBarRender={(action, {selectedRows}) => [
          <Button type="primary">
            <PlusOutlined/> 新建
          </Button>
        ]}
        columns={columns}
        rowSelection={{}}
        request={params => query(params)}
      />
      {
        memoAddPoints
      }
    </PageHeaderWrapper>
  );
};

export default TableList;
