import React, {useRef, useState} from "react";
import {PageHeaderWrapper} from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import {Button, Divider, Modal, message} from "antd";
import AddTaskForm from "@/pages/redBook/zan/components/AddTaskForm";
import {
  addRbZanTask, getZanList,
  startZanTask, pauseZanTask, deleteZanTask
} from '@/services/redbook';

const {confirm} = Modal;

const Zan = () => {
  const actionRef = useRef();

  const handlePause = (id) => {
    confirm({
      title: '警告',
      icon: null,
      content: '确定要暂停吗？',
      onOk() {
        return new Promise((resolve, reject) => {
          pauseZanTask(id).then(res => {
            const {status, msg} = res;
            if (status === 1) {
              message.success(msg);
              actionRef.current.reload();
            } else {
              message.error(msg);
            }
            resolve();
          }).catch(err => {
            reject(err);
          })
        }).catch(() => console.log('Oops errors!'));
      },
      onCancel() {
      },
    });
  }

  const handleStart = (id) => {
    confirm({
      title: '警告',
      icon: null,
      content: '确定要开启吗？',
      onOk() {
        return new Promise((resolve, reject) => {
          startZanTask(id).then(res => {
            const {status, msg} = res;
            if (status === 1) {
              message.success(msg);
              actionRef.current.reload();
            } else {
              message.error(msg);
            }
            resolve();
          }).catch(err => {
            reject(err);
          })
        }).catch(() => console.log('Oops errors!'));
      },
      onCancel() {
      },
    });
  }

  const handleDelete = (id) => {
    confirm({
      title: '警告',
      icon: null,
      content: '确定要删除吗？',
      onOk() {
        return new Promise((resolve, reject) => {
          deleteZanTask(id).then(res => {
            const {status, msg} = res;
            if (status === 1) {
              message.success(msg);
              actionRef.current.reload();
            } else {
              message.error(msg);
            }
            resolve();
          }).catch(err => {
            reject(err);
          })
        }).catch(() => console.log('Oops errors!'));
      },
      onCancel() {
      },
    });
  }

  const columns = [
    {
      title: '地址',
      dataIndex: 'Url'
    },
    {
      title: '点赞数量',
      dataIndex: 'InitNum',
      hideInSearch: true,
    },
    {
      title: '当前点赞数量',
      dataIndex: 'CurrentNum',
      hideInSearch: true,
    },
    {
      title: '状态',
      dataIndex: 'Status',
      hideInSearch: true,
      render: (_, record) => {
        let statusName = ''
        const status = record.Status;
        if (status === 0) {
          statusName = '未工作';
        } else if (status === 1) {
          statusName = '已工作';
        } else if (status === 2) {
          statusName = '已暂停';
        } else {
          statusName = '已完成';
        }
        return (
          <span>{statusName}</span>
        )
      }
    },
    {
      title: '创建时间',
      dataIndex: 'CreateTime',
      valueType: 'dateTime',
      hideInSearch: true
    },
    {
      title: '操作',
      dataIndex: 'option',
      valueType: 'option',
      render: (_, record) => {
        const {Id, Status} = record;
        return (
          <>
            {Status === 0 || Status === 1 ?
              <>
                <Button
                  type="primary" size="small"
                  onClick={() => {
                    handlePause(Id);
                  }}
                >
                  暂停
                </Button>
                <Divider type="vertical"/>
              </>
              : ''}
            {
              Status === 2 ?
                <>
                  <Button
                    type="primary"
                    size="small"
                    onClick={() => handleStart(Id)}
                  >开启</Button>
                  <Divider type="vertical"/>
                  <Button
                    type="primary"
                    size="small"
                    onClick={() => handleDelete(Id)}
                    danger>删除</Button>
                </>
                : ""
            }

          </>
        )
      }
    },
  ];

  const [addVisible, setAddVisible] = useState(false);

  return (
    <PageHeaderWrapper
      breadcrumb={null}
    >
      <ProTable
        columns={columns}
        actionRef={actionRef}
        rowKey={record => record.Id}
        request={params => getZanList(params)}
        options={[]}
        pagination={{
          pageSize: 20,
          showSizeChanger: false
        }}
        toolBarRender={() => [
          <Button type="primary" onClick={() => {
            setAddVisible(true)
          }}>
            新建任务
          </Button>
          // <Button type="default">
          //   暂停
          // </Button>,
          // <Button type="default">
          //   开启
          // </Button>,
          // <Button type="primary" danger>
          //   删除
          // </Button>
        ]}
      />
      <AddTaskForm
        modalVisible={addVisible}
        onCancel={() => setAddVisible(false)}
        onOk={async values => {
          const res = await addRbZanTask(values);
          const {status, msg} = res;
          if (status === 1) {
            message.success(msg);
            setAddVisible(false);
            actionRef.current.reload();
          } else {
            message.error(msg);
          }
        }}
      />
    </PageHeaderWrapper>
  );
}

export default Zan;
