import React, {useState} from "react";
import {Modal, Form, Input, InputNumber, DatePicker } from 'antd';

const FormItem = Form.Item;
const formLayout = {
  labelCol: {
    span: 7,
  },
  wrapperCol: {
    span: 13,
  },
};

const AddTaskForm = (props) => {
  const [form] = Form.useForm();
  const {
    modalVisible,
    onCancel: handleCancel,
    onOk: handleOk
  } = props;
  const [beginTime, setBeginTime] = useState('');

  function onChange(date, dateString) {
    setBeginTime(dateString);
  }

  const handleModalOk = async () => {
    let fieldsValue = await form.validateFields();
    fieldsValue = {...fieldsValue, ...{BeginTime: beginTime}};
    handleOk(fieldsValue);
  };

  return (
    <Modal
      destroyOnClose
      title="创建任务"
      visible={modalVisible}
      onCancel={() => {
        form.resetFields();
        handleCancel();
      }}
      onOk={() => handleModalOk()}
    >
      <Form
        {...formLayout}
        form={form}
      >
        <FormItem
          name="Url"
          label="点赞地址"
          rules={[
            {
              required: true,
              message: '请输入地址',
            },
          ]}
        >
          <Input placeholder="地址" />
        </FormItem>
        <FormItem
          name="Num"
          label="点赞数量"
          rules={[
            {
              required: true,
              message: '请输入地址',
            },
          ]}
        >

          <InputNumber placeholder="地址" />
        </FormItem>

        <FormItem
          name="beginTime"
          label="开始时间"
        >

          <DatePicker onChange={onChange} />
        </FormItem>
      </Form>
    </Modal>
  );
}

export default AddTaskForm;
