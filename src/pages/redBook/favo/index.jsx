import React, {useRef, useState} from "react";

import { PageHeaderWrapper } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import {Button, Divider, Input} from "antd";
import AddFavoTaskForm from "@/pages/redBook/favo/components/AddFavoTaskForm";


const Shoucang = () => {
  const actionRef = useRef();
  const columns = [
    // {
    //   title: '用户',
    //   dataIndex: 'name',
    // },
    {
      title: '地址',
      dataIndex: 'desc',
      valueType: 'textarea',
    },
    {
      title: '收藏数量',
      dataIndex: 'callNo',
      hideInSearch: true,
      renderText: val => `${val} 万`,
    },
    {
      title: '当前收藏数量',
      dataIndex: 'callNo',
      hideInSearch: true,
      renderText: val => `${val} 万`,
    },
    {
      title: '创建时间',
      dataIndex: 'updatedAt',
      valueType: 'dateTime',
      renderFormItem: (item, { defaultRender, ...rest }, form) => {
        const status = form.getFieldValue('status');

        if (`${status}` === '0') {
          return false;
        }

        if (`${status}` === '3') {
          return <Input {...rest} placeholder="请输入异常原因！" />;
        }

        return defaultRender(item);
      },
    },
    {
      title: '操作',
      dataIndex: 'option',
      valueType: 'option',
      render: (_, record) => (
        <>
          <a
            onClick={() => {
            }}
          >
            配置
          </a>
          <Divider type="vertical" />
          <a href="">订阅警报</a>
        </>
      ),
    },
  ];

  const [addVisible, setAddVisible] = useState(false);
  return (
    <PageHeaderWrapper
      breadcrumb={null}
    >
      <ProTable
        columns={columns}
        actionRef={actionRef}
        options={[]}
        toolBarRender={(action, {selectedRows}) => [
          <Button type="primary" onClick={() => {setAddVisible(true)}}>
            新建任务
          </Button>,
          <Button type="default">
            暂停
          </Button>,
          <Button type="default">
            开启
          </Button>,
          <Button type="primary" danger>
            删除
          </Button>
        ]}
      />
      <AddFavoTaskForm
        modalVisible={addVisible}
        onCancel={() => setAddVisible(false)}
        onOk={() => setAddVisible(false)}
      />
    </PageHeaderWrapper>
  );
}

export default Shoucang;
