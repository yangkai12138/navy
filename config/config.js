// https://umijs.org/config/
import { defineConfig } from 'umi';
import defaultSettings from './defaultSettings';
import proxy from './proxy';

const { REACT_APP_ENV } = process.env;
export default defineConfig({
  hash: true,
  antd: {},
  dva: {
    hmr: true,
  },
  locale: {
    // default zh-CN
    default: 'zh-CN',
    // default true, when it is true, will use `navigator.language` overwrite default
    antd: true,
    baseNavigator: true,
  },
  dynamicImport: {
    loading: '@/components/PageLoading/index',
  },
  targets: {
    ie: 11,
  },
  // umi routes: https://umijs.org/docs/routing
  routes: [
    {
      path: '/user',
      component: '../layouts/UserLayout',
      routes: [
        {
          name: 'login',
          path: '/user/login',
          component: './user/login',
        },
      ],
    },
    {
      path: '/',
      component: '../layouts/SecurityLayout',
      routes: [
        {
          path: '/',
          component: '../layouts/BasicLayout',
          routes: [
            {
              path: '/',
              redirect: '/welcome',
            },
            {
              path: '/welcome',
              name: '首页',
              // icon: 'home',
              component: './Welcome',
            },
            {
              path: '/redBook',
              name: '小红书',
              // icon: 'crown',
              routes: [
                {
                  path: '/redBook/zan',
                  name: '小红书点赞',
                  // icon: 'smile',
                  component: './redBook/zan',
                },
                {
                  path: '/redBook/favo',
                  name: '小红书收藏',
                  // icon: 'smile',
                  component: './redBook/favo',
                },
                {
                  component: './404',
                }
              ],
            },
            {
              name: '用户管理',
              // icon: 'setting',
              path: '/list',
              component: './ListTableList',
              authority: ['admin']
            },
            // {
            //   name: '留言板',
            //   // icon: 'setting',
            //   path: '/message',
            //   component: './message',
            // },
            {
              component: './404',
            },
          ],
        },
        {
          component: './404',
        },
      ],
    },
    {
      component: './404',
    },
  ],
  // Theme for antd: https://ant.design/docs/react/customize-theme-cn
  theme: {
    // ...darkTheme,
    'primary-color': defaultSettings.primaryColor,
  },
  // @ts-ignore
  title: false,
  ignoreMomentLocale: true,
  proxy: proxy[REACT_APP_ENV || 'dev'],
  manifest: {
    basePath: '/',
  },
});
